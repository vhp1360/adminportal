[toc]

<div dir="rtl">بنام خدا</div>

# DataBase

## general fields

there are some _fileds_ we should create for every **tables**

```ruby
acl :ineger
user :string
note :text
```



## Tables

```ruby
rails generate scaffold ServerMainInfo srvName:string fdqn:string mainIP:string iphistory_id:integer macAddr:string ip1:string ip2:string ip3:string mac_1:string mac_2:string mac_3:string srvNameInProj:string srvPersianName:string sitename_id:integer osinfo_id:integer srvNameOnVM:string machinetype_id:integer serverstate_id:integer cpuInfo:string ramInfo:string hddInfo:string validIP:string validFqdn:string subnetMask:string gateWay:string projName:string reponseUnit_id:integer responseOrg_id:integer responseName:string responsePhone:string  responseMail:string acl:integer whois_id:integer servergroupinfo_id:integer

rails generate scaffold IPHistory servermaininfo_id:integer ip:string note:text acl:integer whois_id:integer

rails generate scaffold SiteName siteName:string note:text acl:integer whois_id:integer

rails generate scaffold OSInfo osName:string osVersion:string note:text acl:integer whois_id:integer

rails generate scaffold MachineType machineType:string note:text acl:integer whois_id:integer

rails generate scaffold ServerState srvState:string note:text acl:integer whois_id:integer

rails generate scaffold ServerGroupInfo grpName:string grp_id:integer note:text acl:integer whois_id:integer

rails generate scaffold Partitioning prtName:string dvcName:string totalSpace:string usedSpace:string totalINode:String usedInode:string servermaininfo_id:integer note:text acl:integer whois_id:integer

rails generate scaffold ImportantConfFile cfgFileName:string filePath:string servermaininfo_id:integer note:text acl:integer
 whois_id:integer

rails generate scaffold Application appName:string appTpe:string developer:string version:string serviceName:string appUrl:string contextRoot:string dbconnection_id:integer note:text acl:integer whois_id:integer

rails generate scaffold DBConnection jndiName:string dbUserName:string dbName:string dbPort:integer dbIP:string servermaininfo_id:integer note:text acl:integer whois_id:integer

rails generate scaffold RolesName roleName:string whois_id:integer

rails generate scaffold Roles rolesname_id:integer tablesname_id:integer acl:integer whois_id:integer

rails generate scaffold Users firstName:string lastName:string userID:integer whois_id:integer

```



## Generate Migration

1. generate migration

   ```ruby
   rails g migration add_whois_id_filed
   ```

2.  list all tables

   ```ruby
   ActiveRecord::Base.connection.tables
   ```

3. example

   ```ruby
   lass AddWhoisIdFiled < ActiveRecord::Migration[6.1]
     def change
       # [:server_main_infos, :ip_histories, :site_names, :os_infos, ]
       ActiveRecord::Base.connection.tables.each do |t|
         add_column :t ,:whois_id, :integer
       end
     end
     end
   end
   ```

   

4.  Add initialize row: for example. it should add to _migration rn file_

   ```ruby
   ServerGroupInfo.create :grpName => 'Root', :grp_id => 0
   ```

   



# Bootstrap

## Setup Guide

[here](https://bootrails.com/blog/rails-bootstrap-tutorial)

## Navbar

to DRY i define a `@nav_bar` variable, but couldnot find place to put it in **controller**. actually, when put it in _application\_controller_ it raise an error on _routing_ and when put it in other _controller_ it only worked for that page, so put it in **application.erb.html**.|

```ruby
@nav_uris= {:wServerMainTitle => [server_main_infos_treeview_path,new_server_main_info_path], :wSiteNameTitle => [site_names_path,new_site_name_path],:wOSInfoTitle => [os_infos_path,new_os_info_path], :wMachineTypeTitle => [machine_types_path,new_machine_type_path],:wServerStateTitle => [server_states_path,new_server_state_path], :wServerGroupInfoTitle => [server_group_infos_path,new_server_group_info_path]}
```

then use it like below

```html
<% @nav_uris.each do |k,v| %>
  <li class="nav-item dropdown">
    <%= link_to (t k), v[0], class: "nav-link dropdown-toggle",id:"navbarDropdown",role:"button",
                "data-bs-toggle": "dropdown", "aria-expanded": "false" %>
    <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
      <li><%= link_to (t :wList), v[0], class:"dropdown-item" %></li>
      <li><%= link_to (t :wNew2), v[1], class:"dropdown-item" %></li>
    </ul>
  </li>
<% end %>
```







# CLI

```ruby
echo "hostname=$(hostname -s),ip=$(hostname -I | tr ' ' '\n' | grep 192 | head -1),mac=$(cat /sys/class/net/e*/address| head -1),fqdn=$(hostname)"

```







# Rails Commands

## Generate



## Console

- Column Rename

  ```ruby
  ActiveRecord::BASE.connection.rename_column :tblName, :oldCol, :newCol
  ```

  



## dbConsole

```
ALTER TblName RENAME col1 to col2
```

- Import From CSV sample:

  ```plsql
  \copy server_main_infos(servergroupinfo_id,"srvName","srvPersianName",machinetype_id,"cpuInfo","ramInfo","hddInfo",serverstate_id,osinfo_id,"mainIP",note,"macAddr",sitename_id,created_at,updated_at) FROM '/media/Windows2/Ror/AdminPortal/SrvSample.csv' DELIMITER ',' CSV HEADER;
  ```

  







# AjaX

## XMLHttpRequest Object Methods

| Method                            | Description                                                  |
| :-------------------------------- | :----------------------------------------------------------- |
| new XMLHttpRequest()              | Creates a new XMLHttpRequest object                          |
| abort()                           | Cancels the current request                                  |
| getAllResponseHeaders()           | Returns header information                                   |
| getResponseHeader()               | Returns specific header information                          |
| open(*method,url,async,user,psw*) | Specifies the request  *method*: the request type GET or POST *url*: the file location *async*: true (asynchronous) or false (synchronous) *user*: optional user name *psw*: optional password |
| send()                            | Sends the request to the server Used for GET requests        |
| send(*string*)                    | Sends the request to the server. Used for POST requests      |
| setRequestHeader()                | Adds a label/value pair to the header to be sent             |

## The onreadystatechange Property

| Property           | Description                                                  |
| :----------------- | :----------------------------------------------------------- |
| onreadystatechange | Defines a function to be called when the readyState property changes |
| readyState         | Holds the status of the XMLHttpRequest. 0: request not initialized 1: server connection established 2: request received 3: processing request 4: request finished and response is ready |
| status             | 200: "OK" 403: "Forbidden" 404: "Page not found" For a complete list go to the [Http Messages Reference](https://www.w3schools.com/tags/ref_httpmessages.asp) |
| statusText         | Returns the status-text (e.g. "OK" or "Not Found")           |

### More Complete codes

#### 1xx: Information

| Message:                | Description:                                                 |
| :---------------------- | :----------------------------------------------------------- |
| 100 Continue            | The server has received the request headers, and the client should proceed to send the request body |
| 101 Switching Protocols | The requester has asked the server to switch protocols       |
| 103 Checkpoint          | Used in the resumable requests proposal to resume aborted PUT or POST requests |

#### 2xx: Successful

| Message:                          | Description:                                                 |
| :-------------------------------- | :----------------------------------------------------------- |
| 200 OK                            | The request is OK (this is the standard response for successful HTTP requests) |
| 201 Created                       | The request has been fulfilled, and a new resource is created |
| 202 Accepted                      | The request has been accepted for processing, but the processing has not been completed |
| 203 Non-Authoritative Information | The request has been successfully processed, but is returning information that may be from another source |
| 204 No Content                    | The request has been successfully processed, but is not returning any content |
| 205 Reset Content                 | The request has been successfully processed, but is not returning any content, and requires that the requester reset the document view |
| 206 Partial Content               | The server is delivering only part of the resource due to a range header sent by the client |

#### 3xx: Redirection

| Message:               | Description:                                                 |
| :--------------------- | :----------------------------------------------------------- |
| 300 Multiple Choices   | A link list. The user can select a link and go to that location. Maximum five addresses |
| 301 Moved Permanently  | The requested page has moved to a new URL                    |
| 302 Found              | The requested page has moved temporarily to a new URL        |
| 303 See Other          | The requested page can be found under a different URL        |
| 304 Not Modified       | Indicates the requested page has not been modified since last requested |
| 306 Switch Proxy       | *No longer used*                                             |
| 307 Temporary Redirect | The requested page has moved temporarily to a new URL        |
| 308 Resume Incomplete  | Used in the resumable requests proposal to resume aborted PUT or POST requests |

------

------

#### 4xx: Client Error

| Message:                            | Description:                                                 |
| :---------------------------------- | :----------------------------------------------------------- |
| 400 Bad Request                     | The request cannot be fulfilled due to bad syntax            |
| 401 Unauthorized                    | The request was a legal request, but the server is refusing to respond to it. For use when authentication is possible but has failed or not yet been provided |
| 402 Payment Required                | *Reserved for future use*                                    |
| 403 Forbidden                       | The request was a legal request, but the server is refusing to respond to it |
| 404 Not Found                       | The requested page could not be found but may be available again in the future |
| 405 Method Not Allowed              | A request was made of a page using a request method not supported by that page |
| 406 Not Acceptable                  | The server can only generate a response that is not accepted by the client |
| 407 Proxy Authentication Required   | The client must first authenticate itself with the proxy     |
| 408 Request Timeout                 | The server timed out waiting for the request                 |
| 409 Conflict                        | The request could not be completed because of a conflict in the request |
| 410 Gone                            | The requested page is no longer available                    |
| 411 Length Required                 | Thhttps://rubygems.org/e "Content-Length" is not defined. The server will not accept the request without it |
| 412 Precondition Failed             | The precondition given in the request evaluated to false by the server |
| 413 Request Entity Too Large        | The server will not accept the request, because the request entity is too large |
| 414 Request-URI Too Long            | The server will not accept the request, because the URL is too long. Occurs when you convert a POST request to a GET request with a long query information |
| 415 Unsupported Media Type          | The server will not accept the request, because the media type is not supported |
| 416 Requested Range Not Satisfiable | The client has asked for a portion of the file, but the server cannot supply that portion |
| 417 Expectation Failed              | The server cannot meet the requirements of the Expect request-header field |

#### 5xx: Server Error

| Message:                            | Description:                                                 |
| :---------------------------------- | :----------------------------------------------------------- |
| 500 Internal Server Error           | A generic error message, given when no more specific message is suitable |
| 501 Not Implemented                 | The server either does not recognize the request method, or it lacks the ability to fulfill the request |
| 502 Bad Gateway                     | The server was acting as a gateway or proxy and received an invalid response from the upstream server |
| 503 Service Unavailable             | The server is currently unavailable (overloaded or down)     |
| 504 Gateway Timeout                 | The server was acting as a gateway or proxy and did not receive a timely response from the upstream server |
| 505 HTTP Version Not Supported      | The server does not support the HTTP protocol version used in the request |
| 511 Network Authentication Required | The client needs to authenticate to gain network access      |



## Server Response Properties

| Property                         | Description                       |
| :------------------------------- | :-------------------------------- |
| responseText                     | get the response data as a string |
| responseXMLhttps://rubygems.org/ | get the response data as XML data |

------

## Server Response Methods

| Method                  | Description                                                  |
| :---------------------- | :----------------------------------------------------------- |
| getResponseHeader()     | Returns specific header information from the server resource |
| getAllResponseHeaders() | Returns all the header information from the server resource  |

#### UJS

| Event name        | Extra parameters (event.detail) | Fired                                                        |
| :---------------- | :------------------------------ | :----------------------------------------------------------- |
| `ajax:before`     |                                 | Before the whole ajax business.                              |
| `ajax:beforeSend` | [xhr, options]                  | Before the request is sent.                                  |
| `ajax:send`       | [xhr]                           | When the request is sent.                                    |
| `ajax:stopped`    |                                 | When the request is stopped.                                 |
| `ajax:success`    | [response, status, xhr]         | After completion, if the response was a success.             |
| `ajax:error`      | [response, status, xhr]         | After completion, if the response was an error.              |
| `ajax:complete`   | [xhr, status]                   | After the request has been completed, no matter the outcome. |

### Master Details 

I add new page to this issue: **treeview** _action_ in **server_main_infos** _controller_

1. _the first issue_ is **jQuery configs**. as _rails6_ used _webpack_, it is different from preveious:

   - in `config/webpack/environment.js` we have:

     ```js
     const { environment } = require('@rails/webpacker')
     const webpack = require('webpack')
     environment.plugins.prepend('Provide', new webpack.ProvidePlugin({
         $: 'jquery/src/jquery',
         jQuery: 'jquery/src/jquery',
         jquery: 'jquery',
         'window.jQuery': 'jquery',
         Popper: ['popper.js', 'default']
     }))
     module.exports = environment
     ```

   - remove `require('jquery')` from _app/frontend/packs/application.js_

   - and below in that file:

     ```ruby
     import Rails from "@rails/ujs"
     import Turbolinks from "turbolinks"
     import * as ActiveStorage from "@rails/activestorage"
     import "channels"
     import '../js/bootstrap_js_files.js'
     
     Rails.start()
     // Turbolinks.start()
     ActiveStorage.start()
     ```

   - also may you need add _jQuery_ with `yarn add jquery`

2.  in **treeview.html.erb**  I add tow main _div_ on as _sidebar_ and second for _partial render_:

   1. in first div:

      ```erb
      <div class="overflow-scroll">
       <ul class="nav nav-pills flex-column mb-sm-auto mb-0 align-items-center align-items-sm-start" id="menu">
        <li class="nav-item">
         <% $server_main_infos.each do |server_main_info| %>
          <%= link_to treeview_item_server_main_info_path(server_main_info), class:"nav-link align-middle px-0", remote: true do %>
           <em class="fs-4 bi-server"></em> <span class="ms-1 d-none d-sm-inline"><%= server_main_info.srvName %></span>
          <% end %>
         <% end %>>
        </li>
       </ul>
      </div>
      ```

      

   2.  and in second one:

      ```erb
      <div class="col py-3" id="load_partial_tree"></div>
      ```

      - Note: **id** is important

3. as you see in the _first div_ I have `treeview_item_server_main_info` route, so we need view of itself. however, in continuation of that code we have `remote: true`, so it should be as **js** (_app/views/server_main_infos/treeview_item.js.erb_):

   ```erb
   $("#load_partial_tree").html("<%= escape_javascript( render partial: 'treeview', locals: { server_main_info: @treeviewItem} ) %>")
   ```

   

4.  in addition, `partial: 'treeview'` sugesst that we create **Partial View**. it should have **_** and *same name of action*(_treeview.html.erb_):

   ```elixir
   in this View, We add all properties and filed we would, Just like other erb.html files
   ```

   

5.  in **server_main_infos_controller** add two **action**:

   1. first:

      ```ruby
      def treeview
        @server_main_infos = ServerMainInfo.all
      end
      ```

   2.  second:

      ```ruby
      def treeview_item
        @treeviewItem= ServerMainInfo.find(params[:id])
        respond_to do |format|
          format.js { render layout: false }
      end
      ```

6. so far,the majority of the work has been completed. finally provide routing:

   ```ruby
   resources :server_main_infos do
     get 'treeview', on: :collection  <-- make route as : server_main_infos/treeview
     get 'treeview_item', on: :member <-- make route as : server_main_infos/:id/treeview_item
   ```

   





# HTML issues

#### Selector

```html
  <div class="field">
    <%= form.label :servergroupinfo_id %>
    <%= form.collection_select :servergroupinfo_id  , ServerGroupInfo.all,:id, :grpName, :selected => params[:servergroupinfo_id] %>
  </div>
```





# Route

- alert
  [4.7 Translated Paths](https://guides.rubyonrails.org/routing.html#translated-paths)

  Using `scope`, we can alter path names generated by `resources`:

  ```
  scope(path_names: { new: 'neu', edit: 'bearbeiten' }) do
    resources :categories, path: 'kategorien'
  end
  ```

  Copy

  Rails now creates routes to the `CategoriesController`.

  | HTTP Verb | Path                       | Controller#Action  | Named Route Helper      |
  | :-------- | :------------------------- | :----------------- | :---------------------- |
  | GET       | /kategorien                | categories#index   | categories_path         |
  | GET       | /kategorien/neu            | categories#new     | new_category_path       |
  | POST      | /kategorien                | categories#create  | categories_path         |
  | GET       | /kategorien/:id            | categories#show    | category_path(:id)      |
  | GET       | /kategorien/:id/bearbeiten | categories#edit    | edit_category_path(:id) |
  | PATCH/PUT | /kategorien/:id            | categories#update  | category_path(:id)      |
  | DELETE    | /kategorien/:id            | categories#destroy | category_path(:id)      |

## Custom Route

if we add _custom action_ to a controller, to add new route:

```ruby
resources somteTable do
  get 'customeAction', on: :collection
```

this would _add new route_ like **`someTable/customAction`** and refer to it with: `customAction_someTables_path`






