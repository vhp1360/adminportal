require "application_system_test_case"

class SiteNamesTest < ApplicationSystemTestCase
  setup do
    @site_name = site_names(:one)
  end

  test "visiting the index" do
    visit site_names_url
    assert_selector "h1", text: "Site Names"
  end

  test "creating a Site name" do
    visit site_names_url
    click_on "New Site Name"

    fill_in "Note", with: @site_name.note
    fill_in "Sitename", with: @site_name.siteName
    click_on "Create Site name"

    assert_text "Site name was successfully created"
    click_on "Back"
  end

  test "updating a Site name" do
    visit site_names_url
    click_on "Edit", match: :first

    fill_in "Note", with: @site_name.note
    fill_in "Sitename", with: @site_name.siteName
    click_on "Update Site name"

    assert_text "Site name was successfully updated"
    click_on "Back"
  end

  test "destroying a Site name" do
    visit site_names_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Site name was successfully destroyed"
  end
end
