require "application_system_test_case"

class ServerStatesTest < ApplicationSystemTestCase
  setup do
    @server_state = server_states(:one)
  end

  test "visiting the index" do
    visit server_states_url
    assert_selector "h1", text: "Server States"
  end

  test "creating a Server state" do
    visit server_states_url
    click_on "New Server State"

    fill_in "Note", with: @server_state.note
    fill_in "Srvstate", with: @server_state.srvState
    click_on "Create Server state"

    assert_text "Server state was successfully created"
    click_on "Back"
  end

  test "updating a Server state" do
    visit server_states_url
    click_on "Edit", match: :first

    fill_in "Note", with: @server_state.note
    fill_in "Srvstate", with: @server_state.srvState
    click_on "Update Server state"

    assert_text "Server state was successfully updated"
    click_on "Back"
  end

  test "destroying a Server state" do
    visit server_states_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Server state was successfully destroyed"
  end
end
