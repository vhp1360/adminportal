require "application_system_test_case"

class ServerGroupInfosTest < ApplicationSystemTestCase
  setup do
    @server_group_info = server_group_infos(:one)
  end

  test "visiting the index" do
    visit server_group_infos_url
    assert_selector "h1", text: "Server Group Infos"
  end

  test "creating a Server group info" do
    visit server_group_infos_url
    click_on "New Server Group Info"

    fill_in "Grpname", with: @server_group_info.grpName
    fill_in "Grp", with: @server_group_info.grp_id
    fill_in "Note", with: @server_group_info.note
    click_on "Create Server group info"

    assert_text "Server group info was successfully created"
    click_on "Back"
  end

  test "updating a Server group info" do
    visit server_group_infos_url
    click_on "Edit", match: :first

    fill_in "Grpname", with: @server_group_info.grpName
    fill_in "Grp", with: @server_group_info.grp_id
    fill_in "Note", with: @server_group_info.note
    click_on "Update Server group info"

    assert_text "Server group info was successfully updated"
    click_on "Back"
  end

  test "destroying a Server group info" do
    visit server_group_infos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Server group info was successfully destroyed"
  end
end
