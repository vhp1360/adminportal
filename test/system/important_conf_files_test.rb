require "application_system_test_case"

class ImportantConfFilesTest < ApplicationSystemTestCase
  setup do
    @important_conf_file = important_conf_files(:one)
  end

  test "visiting the index" do
    visit important_conf_files_url
    assert_selector "h1", text: "Important Conf Files"
  end

  test "creating a Important conf file" do
    visit important_conf_files_url
    click_on "New Important Conf File"

    fill_in "Cfgfilename", with: @important_conf_file.cfgFileName
    fill_in "Filepath", with: @important_conf_file.filePath
    fill_in "Note", with: @important_conf_file.note
    fill_in "Servermaininfo", with: @important_conf_file.servermaininfo_id
    click_on "Create Important conf file"

    assert_text "Important conf file was successfully created"
    click_on "Back"
  end

  test "updating a Important conf file" do
    visit important_conf_files_url
    click_on "Edit", match: :first

    fill_in "Cfgfilename", with: @important_conf_file.cfgFileName
    fill_in "Filepath", with: @important_conf_file.filePath
    fill_in "Note", with: @important_conf_file.note
    fill_in "Servermaininfo", with: @important_conf_file.servermaininfo_id
    click_on "Update Important conf file"

    assert_text "Important conf file was successfully updated"
    click_on "Back"
  end

  test "destroying a Important conf file" do
    visit important_conf_files_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Important conf file was successfully destroyed"
  end
end
