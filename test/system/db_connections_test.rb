require "application_system_test_case"

class DbConnectionsTest < ApplicationSystemTestCase
  setup do
    @db_connection = db_connections(:one)
  end

  test "visiting the index" do
    visit db_connections_url
    assert_selector "h1", text: "Db Connections"
  end

  test "creating a Db connection" do
    visit db_connections_url
    click_on "New Db Connection"

    fill_in "Dbip", with: @db_connection.dbIP
    fill_in "Dbname", with: @db_connection.dbName
    fill_in "Dbport", with: @db_connection.dbPort
    fill_in "Dbusername", with: @db_connection.dbUserName
    fill_in "Jndiname", with: @db_connection.jndiName
    fill_in "Note", with: @db_connection.note
    fill_in "Servermaininfo", with: @db_connection.servermaininfo_id
    click_on "Create Db connection"

    assert_text "Db connection was successfully created"
    click_on "Back"
  end

  test "updating a Db connection" do
    visit db_connections_url
    click_on "Edit", match: :first

    fill_in "Dbip", with: @db_connection.dbIP
    fill_in "Dbname", with: @db_connection.dbName
    fill_in "Dbport", with: @db_connection.dbPort
    fill_in "Dbusername", with: @db_connection.dbUserName
    fill_in "Jndiname", with: @db_connection.jndiName
    fill_in "Note", with: @db_connection.note
    fill_in "Servermaininfo", with: @db_connection.servermaininfo_id
    click_on "Update Db connection"

    assert_text "Db connection was successfully updated"
    click_on "Back"
  end

  test "destroying a Db connection" do
    visit db_connections_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Db connection was successfully destroyed"
  end
end
