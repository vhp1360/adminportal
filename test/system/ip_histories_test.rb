require "application_system_test_case"

class IpHistoriesTest < ApplicationSystemTestCase
  setup do
    @ip_history = ip_histories(:one)
  end

  test "visiting the index" do
    visit ip_histories_url
    assert_selector "h1", text: "Ip Histories"
  end

  test "creating a Ip history" do
    visit ip_histories_url
    click_on "New Ip History"

    fill_in "Ip", with: @ip_history.ip
    fill_in "Note", with: @ip_history.note
    fill_in "Servermaininfo", with: @ip_history.servermaininfo_id
    click_on "Create Ip history"

    assert_text "Ip history was successfully created"
    click_on "Back"
  end

  test "updating a Ip history" do
    visit ip_histories_url
    click_on "Edit", match: :first

    fill_in "Ip", with: @ip_history.ip
    fill_in "Note", with: @ip_history.note
    fill_in "Servermaininfo", with: @ip_history.servermaininfo_id
    click_on "Update Ip history"

    assert_text "Ip history was successfully updated"
    click_on "Back"
  end

  test "destroying a Ip history" do
    visit ip_histories_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Ip history was successfully destroyed"
  end
end
