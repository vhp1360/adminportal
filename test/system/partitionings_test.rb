require "application_system_test_case"

class PartitioningsTest < ApplicationSystemTestCase
  setup do
    @partitioning = partitionings(:one)
  end

  test "visiting the index" do
    visit partitionings_url
    assert_selector "h1", text: "Partitionings"
  end

  test "creating a Partitioning" do
    visit partitionings_url
    click_on "New Partitioning"

    fill_in "Dvcname", with: @partitioning.dvcName
    fill_in "Note", with: @partitioning.note
    fill_in "Prtname", with: @partitioning.prtName
    fill_in "Servermaininfo", with: @partitioning.servermaininfo_id
    fill_in "Totalinode", with: @partitioning.totalINode
    fill_in "Totalspace", with: @partitioning.totalSpace
    fill_in "Usedinode", with: @partitioning.usedInode
    fill_in "Usedspace", with: @partitioning.usedSpace
    click_on "Create Partitioning"

    assert_text "Partitioning was successfully created"
    click_on "Back"
  end

  test "updating a Partitioning" do
    visit partitionings_url
    click_on "Edit", match: :first

    fill_in "Dvcname", with: @partitioning.dvcName
    fill_in "Note", with: @partitioning.note
    fill_in "Prtname", with: @partitioning.prtName
    fill_in "Servermaininfo", with: @partitioning.servermaininfo_id
    fill_in "Totalinode", with: @partitioning.totalINode
    fill_in "Totalspace", with: @partitioning.totalSpace
    fill_in "Usedinode", with: @partitioning.usedInode
    fill_in "Usedspace", with: @partitioning.usedSpace
    click_on "Update Partitioning"

    assert_text "Partitioning was successfully updated"
    click_on "Back"
  end

  test "destroying a Partitioning" do
    visit partitionings_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Partitioning was successfully destroyed"
  end
end
