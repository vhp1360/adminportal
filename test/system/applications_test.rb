require "application_system_test_case"

class ApplicationsTest < ApplicationSystemTestCase
  setup do
    @application = applications(:one)
  end

  test "visiting the index" do
    visit applications_url
    assert_selector "h1", text: "Applications"
  end

  test "creating a Application" do
    visit applications_url
    click_on "New Application"

    fill_in "Appname", with: @application.appName
    fill_in "Apptpe", with: @application.appTpe
    fill_in "Appurl", with: @application.appUrl
    fill_in "Contextroot", with: @application.contextRoot
    fill_in "Dbconnection", with: @application.dbconnection_id
    fill_in "Developer", with: @application.developer
    fill_in "Note", with: @application.note
    fill_in "Servicename", with: @application.serviceName
    fill_in "Version", with: @application.version
    click_on "Create Application"

    assert_text "Application was successfully created"
    click_on "Back"
  end

  test "updating a Application" do
    visit applications_url
    click_on "Edit", match: :first

    fill_in "Appname", with: @application.appName
    fill_in "Apptpe", with: @application.appTpe
    fill_in "Appurl", with: @application.appUrl
    fill_in "Contextroot", with: @application.contextRoot
    fill_in "Dbconnection", with: @application.dbconnection_id
    fill_in "Developer", with: @application.developer
    fill_in "Note", with: @application.note
    fill_in "Servicename", with: @application.serviceName
    fill_in "Version", with: @application.version
    click_on "Update Application"

    assert_text "Application was successfully updated"
    click_on "Back"
  end

  test "destroying a Application" do
    visit applications_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Application was successfully destroyed"
  end
end
