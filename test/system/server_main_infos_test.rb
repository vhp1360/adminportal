require "application_system_test_case"

class ServerMainInfosTest < ApplicationSystemTestCase
  setup do
    @server_main_info = server_main_infos(:one)
  end

  test "visiting the index" do
    visit server_main_infos_url
    assert_selector "h1", text: "Server Main Infos"
  end

  test "creating a Server main info" do
    visit server_main_infos_url
    click_on "New Server Main Info"

    fill_in "Cpuinfo", with: @server_main_info.cpuInfo
    fill_in "Fdqn", with: @server_main_info.fdqn
    fill_in "Gateway", with: @server_main_info.gateWay
    fill_in "Hddinfo", with: @server_main_info.hddInfo
    fill_in "Ip1", with: @server_main_info.ip1
    fill_in "Ip2", with: @server_main_info.ip2
    fill_in "Ip3", with: @server_main_info.ip3
    fill_in "Iphistory", with: @server_main_info.iphistory_id
    fill_in "Macaddr", with: @server_main_info.macAddr
    fill_in "Mac 1", with: @server_main_info.mac_1
    fill_in "Mac 2", with: @server_main_info.mac_2
    fill_in "Mac 3", with: @server_main_info.mac_3
    fill_in "Machinetype", with: @server_main_info.machinetype_id
    fill_in "Mainip", with: @server_main_info.mainIP
    fill_in "Osinfo", with: @server_main_info.osinfo_id
    fill_in "Projname", with: @server_main_info.projName
    fill_in "Raminfo", with: @server_main_info.ramInfo
    fill_in "Reponseunit", with: @server_main_info.reponseUnit_id
    fill_in "Responsemail", with: @server_main_info.responseMail
    fill_in "Responsename", with: @server_main_info.responseName
    fill_in "Responseorg", with: @server_main_info.responseOrg_id
    fill_in "Responsephone", with: @server_main_info.responsePhone
    fill_in "Serverstate", with: @server_main_info.serverstate_id
    fill_in "Sitename", with: @server_main_info.sitename_id
    fill_in "Srvname", with: @server_main_info.srvName
    fill_in "Srvnameinproj", with: @server_main_info.srvNameInProj
    fill_in "Srvnameonvm", with: @server_main_info.srvNameOnVM
    fill_in "Srvpersianname", with: @server_main_info.srvPersianName
    fill_in "Subnetmask", with: @server_main_info.subnetMask
    fill_in "Validfqdn", with: @server_main_info.validFqdn
    fill_in "Validip", with: @server_main_info.validIP
    click_on "Create Server main info"

    assert_text "Server main info was successfully created"
    click_on "Back"
  end

  test "updating a Server main info" do
    visit server_main_infos_url
    click_on "Edit", match: :first

    fill_in "Cpuinfo", with: @server_main_info.cpuInfo
    fill_in "Fdqn", with: @server_main_info.fdqn
    fill_in "Gateway", with: @server_main_info.gateWay
    fill_in "Hddinfo", with: @server_main_info.hddInfo
    fill_in "Ip1", with: @server_main_info.ip1
    fill_in "Ip2", with: @server_main_info.ip2
    fill_in "Ip3", with: @server_main_info.ip3
    fill_in "Iphistory", with: @server_main_info.iphistory_id
    fill_in "Macaddr", with: @server_main_info.macAddr
    fill_in "Mac 1", with: @server_main_info.mac_1
    fill_in "Mac 2", with: @server_main_info.mac_2
    fill_in "Mac 3", with: @server_main_info.mac_3
    fill_in "Machinetype", with: @server_main_info.machinetype_id
    fill_in "Mainip", with: @server_main_info.mainIP
    fill_in "Osinfo", with: @server_main_info.osinfo_id
    fill_in "Projname", with: @server_main_info.projName
    fill_in "Raminfo", with: @server_main_info.ramInfo
    fill_in "Reponseunit", with: @server_main_info.reponseUnit_id
    fill_in "Responsemail", with: @server_main_info.responseMail
    fill_in "Responsename", with: @server_main_info.responseName
    fill_in "Responseorg", with: @server_main_info.responseOrg_id
    fill_in "Responsephone", with: @server_main_info.responsePhone
    fill_in "Serverstate", with: @server_main_info.serverstate_id
    fill_in "Sitename", with: @server_main_info.sitename_id
    fill_in "Srvname", with: @server_main_info.srvName
    fill_in "Srvnameinproj", with: @server_main_info.srvNameInProj
    fill_in "Srvnameonvm", with: @server_main_info.srvNameOnVM
    fill_in "Srvpersianname", with: @server_main_info.srvPersianName
    fill_in "Subnetmask", with: @server_main_info.subnetMask
    fill_in "Validfqdn", with: @server_main_info.validFqdn
    fill_in "Validip", with: @server_main_info.validIP
    click_on "Update Server main info"

    assert_text "Server main info was successfully updated"
    click_on "Back"
  end

  test "destroying a Server main info" do
    visit server_main_infos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Server main info was successfully destroyed"
  end
end
