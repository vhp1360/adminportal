require "application_system_test_case"

class OsInfosTest < ApplicationSystemTestCase
  setup do
    @os_info = os_infos(:one)
  end

  test "visiting the index" do
    visit os_infos_url
    assert_selector "h1", text: "Os Infos"
  end

  test "creating a Os info" do
    visit os_infos_url
    click_on "New Os Info"

    fill_in "Note", with: @os_info.note
    fill_in "Osname", with: @os_info.osName
    fill_in "Osversion", with: @os_info.osVersion
    click_on "Create Os info"

    assert_text "Os info was successfully created"
    click_on "Back"
  end

  test "updating a Os info" do
    visit os_infos_url
    click_on "Edit", match: :first

    fill_in "Note", with: @os_info.note
    fill_in "Osname", with: @os_info.osName
    fill_in "Osversion", with: @os_info.osVersion
    click_on "Update Os info"

    assert_text "Os info was successfully updated"
    click_on "Back"
  end

  test "destroying a Os info" do
    visit os_infos_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Os info was successfully destroyed"
  end
end
