require "application_system_test_case"

class RolesNamesTest < ApplicationSystemTestCase
  setup do
    @roles_name = roles_names(:one)
  end

  test "visiting the index" do
    visit roles_names_url
    assert_selector "h1", text: "Roles Names"
  end

  test "creating a Roles name" do
    visit roles_names_url
    click_on "New Roles Name"

    fill_in "Rolename", with: @roles_name.roleName
    fill_in "Whois", with: @roles_name.whois_id
    click_on "Create Roles name"

    assert_text "Roles name was successfully created"
    click_on "Back"
  end

  test "updating a Roles name" do
    visit roles_names_url
    click_on "Edit", match: :first

    fill_in "Rolename", with: @roles_name.roleName
    fill_in "Whois", with: @roles_name.whois_id
    click_on "Update Roles name"

    assert_text "Roles name was successfully updated"
    click_on "Back"
  end

  test "destroying a Roles name" do
    visit roles_names_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Roles name was successfully destroyed"
  end
end
