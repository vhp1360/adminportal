require "test_helper"

class ImportantConfFilesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @important_conf_file = important_conf_files(:one)
  end

  test "should get index" do
    get important_conf_files_url
    assert_response :success
  end

  test "should get new" do
    get new_important_conf_file_url
    assert_response :success
  end

  test "should create important_conf_file" do
    assert_difference('ImportantConfFile.count') do
      post important_conf_files_url, params: { important_conf_file: { cfgFileName: @important_conf_file.cfgFileName, filePath: @important_conf_file.filePath, note: @important_conf_file.note, servermaininfo_id: @important_conf_file.servermaininfo_id } }
    end

    assert_redirected_to important_conf_file_url(ImportantConfFile.last)
  end

  test "should show important_conf_file" do
    get important_conf_file_url(@important_conf_file)
    assert_response :success
  end

  test "should get edit" do
    get edit_important_conf_file_url(@important_conf_file)
    assert_response :success
  end

  test "should update important_conf_file" do
    patch important_conf_file_url(@important_conf_file), params: { important_conf_file: { cfgFileName: @important_conf_file.cfgFileName, filePath: @important_conf_file.filePath, note: @important_conf_file.note, servermaininfo_id: @important_conf_file.servermaininfo_id } }
    assert_redirected_to important_conf_file_url(@important_conf_file)
  end

  test "should destroy important_conf_file" do
    assert_difference('ImportantConfFile.count', -1) do
      delete important_conf_file_url(@important_conf_file)
    end

    assert_redirected_to important_conf_files_url
  end
end
