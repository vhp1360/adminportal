require "test_helper"

class PartitioningsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @partitioning = partitionings(:one)
  end

  test "should get index" do
    get partitionings_url
    assert_response :success
  end

  test "should get new" do
    get new_partitioning_url
    assert_response :success
  end

  test "should create partitioning" do
    assert_difference('Partitioning.count') do
      post partitionings_url, params: { partitioning: { dvcName: @partitioning.dvcName, note: @partitioning.note, prtName: @partitioning.prtName, servermaininfo_id: @partitioning.servermaininfo_id, totalINode: @partitioning.totalINode, totalSpace: @partitioning.totalSpace, usedInode: @partitioning.usedInode, usedSpace: @partitioning.usedSpace } }
    end

    assert_redirected_to partitioning_url(Partitioning.last)
  end

  test "should show partitioning" do
    get partitioning_url(@partitioning)
    assert_response :success
  end

  test "should get edit" do
    get edit_partitioning_url(@partitioning)
    assert_response :success
  end

  test "should update partitioning" do
    patch partitioning_url(@partitioning), params: { partitioning: { dvcName: @partitioning.dvcName, note: @partitioning.note, prtName: @partitioning.prtName, servermaininfo_id: @partitioning.servermaininfo_id, totalINode: @partitioning.totalINode, totalSpace: @partitioning.totalSpace, usedInode: @partitioning.usedInode, usedSpace: @partitioning.usedSpace } }
    assert_redirected_to partitioning_url(@partitioning)
  end

  test "should destroy partitioning" do
    assert_difference('Partitioning.count', -1) do
      delete partitioning_url(@partitioning)
    end

    assert_redirected_to partitionings_url
  end
end
