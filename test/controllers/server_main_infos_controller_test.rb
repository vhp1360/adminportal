require "test_helper"

class ServerMainInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @server_main_info = server_main_infos(:one)
  end

  test "should get index" do
    get server_main_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_server_main_info_url
    assert_response :success
  end

  test "should create server_main_info" do
    assert_difference('ServerMainInfo.count') do
      post server_main_infos_url, params: { server_main_info: { cpuInfo: @server_main_info.cpuInfo, fdqn: @server_main_info.fdqn, gateWay: @server_main_info.gateWay, hddInfo: @server_main_info.hddInfo, ip1: @server_main_info.ip1, ip2: @server_main_info.ip2, ip3: @server_main_info.ip3, iphistory_id: @server_main_info.iphistory_id, macAddr: @server_main_info.macAddr, mac_1: @server_main_info.mac_1, mac_2: @server_main_info.mac_2, mac_3: @server_main_info.mac_3, machinetype_id: @server_main_info.machinetype_id, mainIP: @server_main_info.mainIP, osinfo_id: @server_main_info.osinfo_id, projName: @server_main_info.projName, ramInfo: @server_main_info.ramInfo, reponseUnit_id: @server_main_info.reponseUnit_id, responseMail: @server_main_info.responseMail, responseName: @server_main_info.responseName, responseOrg_id: @server_main_info.responseOrg_id, responsePhone: @server_main_info.responsePhone, serverstate_id: @server_main_info.serverstate_id, sitename_id: @server_main_info.sitename_id, srvName: @server_main_info.srvName, srvNameInProj: @server_main_info.srvNameInProj, srvNameOnVM: @server_main_info.srvNameOnVM, srvPersianName: @server_main_info.srvPersianName, subnetMask: @server_main_info.subnetMask, validFqdn: @server_main_info.validFqdn, validIP: @server_main_info.validIP } }
    end

    assert_redirected_to server_main_info_url(ServerMainInfo.last)
  end

  test "should show server_main_info" do
    get server_main_info_url(@server_main_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_server_main_info_url(@server_main_info)
    assert_response :success
  end

  test "should update server_main_info" do
    patch server_main_info_url(@server_main_info), params: { server_main_info: { cpuInfo: @server_main_info.cpuInfo, fdqn: @server_main_info.fdqn, gateWay: @server_main_info.gateWay, hddInfo: @server_main_info.hddInfo, ip1: @server_main_info.ip1, ip2: @server_main_info.ip2, ip3: @server_main_info.ip3, iphistory_id: @server_main_info.iphistory_id, macAddr: @server_main_info.macAddr, mac_1: @server_main_info.mac_1, mac_2: @server_main_info.mac_2, mac_3: @server_main_info.mac_3, machinetype_id: @server_main_info.machinetype_id, mainIP: @server_main_info.mainIP, osinfo_id: @server_main_info.osinfo_id, projName: @server_main_info.projName, ramInfo: @server_main_info.ramInfo, reponseUnit_id: @server_main_info.reponseUnit_id, responseMail: @server_main_info.responseMail, responseName: @server_main_info.responseName, responseOrg_id: @server_main_info.responseOrg_id, responsePhone: @server_main_info.responsePhone, serverstate_id: @server_main_info.serverstate_id, sitename_id: @server_main_info.sitename_id, srvName: @server_main_info.srvName, srvNameInProj: @server_main_info.srvNameInProj, srvNameOnVM: @server_main_info.srvNameOnVM, srvPersianName: @server_main_info.srvPersianName, subnetMask: @server_main_info.subnetMask, validFqdn: @server_main_info.validFqdn, validIP: @server_main_info.validIP } }
    assert_redirected_to server_main_info_url(@server_main_info)
  end

  test "should destroy server_main_info" do
    assert_difference('ServerMainInfo.count', -1) do
      delete server_main_info_url(@server_main_info)
    end

    assert_redirected_to server_main_infos_url
  end
end
