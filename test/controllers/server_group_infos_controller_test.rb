require "test_helper"

class ServerGroupInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @server_group_info = server_group_infos(:one)
  end

  test "should get index" do
    get server_group_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_server_group_info_url
    assert_response :success
  end

  test "should create server_group_info" do
    assert_difference('ServerGroupInfo.count') do
      post server_group_infos_url, params: { server_group_info: { grpName: @server_group_info.grpName, grp_id: @server_group_info.grp_id, note: @server_group_info.note } }
    end

    assert_redirected_to server_group_info_url(ServerGroupInfo.last)
  end

  test "should show server_group_info" do
    get server_group_info_url(@server_group_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_server_group_info_url(@server_group_info)
    assert_response :success
  end

  test "should update server_group_info" do
    patch server_group_info_url(@server_group_info), params: { server_group_info: { grpName: @server_group_info.grpName, grp_id: @server_group_info.grp_id, note: @server_group_info.note } }
    assert_redirected_to server_group_info_url(@server_group_info)
  end

  test "should destroy server_group_info" do
    assert_difference('ServerGroupInfo.count', -1) do
      delete server_group_info_url(@server_group_info)
    end

    assert_redirected_to server_group_infos_url
  end
end
