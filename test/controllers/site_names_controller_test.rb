require "test_helper"

class SiteNamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @site_name = site_names(:one)
  end

  test "should get index" do
    get site_names_url
    assert_response :success
  end

  test "should get new" do
    get new_site_name_url
    assert_response :success
  end

  test "should create site_name" do
    assert_difference('SiteName.count') do
      post site_names_url, params: { site_name: { note: @site_name.note, siteName: @site_name.siteName } }
    end

    assert_redirected_to site_name_url(SiteName.last)
  end

  test "should show site_name" do
    get site_name_url(@site_name)
    assert_response :success
  end

  test "should get edit" do
    get edit_site_name_url(@site_name)
    assert_response :success
  end

  test "should update site_name" do
    patch site_name_url(@site_name), params: { site_name: { note: @site_name.note, siteName: @site_name.siteName } }
    assert_redirected_to site_name_url(@site_name)
  end

  test "should destroy site_name" do
    assert_difference('SiteName.count', -1) do
      delete site_name_url(@site_name)
    end

    assert_redirected_to site_names_url
  end
end
