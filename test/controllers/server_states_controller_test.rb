require "test_helper"

class ServerStatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @server_state = server_states(:one)
  end

  test "should get index" do
    get server_states_url
    assert_response :success
  end

  test "should get new" do
    get new_server_state_url
    assert_response :success
  end

  test "should create server_state" do
    assert_difference('ServerState.count') do
      post server_states_url, params: { server_state: { note: @server_state.note, srvState: @server_state.srvState } }
    end

    assert_redirected_to server_state_url(ServerState.last)
  end

  test "should show server_state" do
    get server_state_url(@server_state)
    assert_response :success
  end

  test "should get edit" do
    get edit_server_state_url(@server_state)
    assert_response :success
  end

  test "should update server_state" do
    patch server_state_url(@server_state), params: { server_state: { note: @server_state.note, srvState: @server_state.srvState } }
    assert_redirected_to server_state_url(@server_state)
  end

  test "should destroy server_state" do
    assert_difference('ServerState.count', -1) do
      delete server_state_url(@server_state)
    end

    assert_redirected_to server_states_url
  end
end
