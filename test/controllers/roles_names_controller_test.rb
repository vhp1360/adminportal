require "test_helper"

class RolesNamesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @roles_name = roles_names(:one)
  end

  test "should get index" do
    get roles_names_url
    assert_response :success
  end

  test "should get new" do
    get new_roles_name_url
    assert_response :success
  end

  test "should create roles_name" do
    assert_difference('RolesName.count') do
      post roles_names_url, params: { roles_name: { roleName: @roles_name.roleName, whois_id: @roles_name.whois_id } }
    end

    assert_redirected_to roles_name_url(RolesName.last)
  end

  test "should show roles_name" do
    get roles_name_url(@roles_name)
    assert_response :success
  end

  test "should get edit" do
    get edit_roles_name_url(@roles_name)
    assert_response :success
  end

  test "should update roles_name" do
    patch roles_name_url(@roles_name), params: { roles_name: { roleName: @roles_name.roleName, whois_id: @roles_name.whois_id } }
    assert_redirected_to roles_name_url(@roles_name)
  end

  test "should destroy roles_name" do
    assert_difference('RolesName.count', -1) do
      delete roles_name_url(@roles_name)
    end

    assert_redirected_to roles_names_url
  end
end
