require "test_helper"

class OsInfosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @os_info = os_infos(:one)
  end

  test "should get index" do
    get os_infos_url
    assert_response :success
  end

  test "should get new" do
    get new_os_info_url
    assert_response :success
  end

  test "should create os_info" do
    assert_difference('OsInfo.count') do
      post os_infos_url, params: { os_info: { note: @os_info.note, osName: @os_info.osName, osVersion: @os_info.osVersion } }
    end

    assert_redirected_to os_info_url(OsInfo.last)
  end

  test "should show os_info" do
    get os_info_url(@os_info)
    assert_response :success
  end

  test "should get edit" do
    get edit_os_info_url(@os_info)
    assert_response :success
  end

  test "should update os_info" do
    patch os_info_url(@os_info), params: { os_info: { note: @os_info.note, osName: @os_info.osName, osVersion: @os_info.osVersion } }
    assert_redirected_to os_info_url(@os_info)
  end

  test "should destroy os_info" do
    assert_difference('OsInfo.count', -1) do
      delete os_info_url(@os_info)
    end

    assert_redirected_to os_infos_url
  end
end
