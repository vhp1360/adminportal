require "test_helper"

class IpHistoriesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @ip_history = ip_histories(:one)
  end

  test "should get index" do
    get ip_histories_url
    assert_response :success
  end

  test "should get new" do
    get new_ip_history_url
    assert_response :success
  end

  test "should create ip_history" do
    assert_difference('IpHistory.count') do
      post ip_histories_url, params: { ip_history: { ip: @ip_history.ip, note: @ip_history.note, servermaininfo_id: @ip_history.servermaininfo_id } }
    end

    assert_redirected_to ip_history_url(IpHistory.last)
  end

  test "should show ip_history" do
    get ip_history_url(@ip_history)
    assert_response :success
  end

  test "should get edit" do
    get edit_ip_history_url(@ip_history)
    assert_response :success
  end

  test "should update ip_history" do
    patch ip_history_url(@ip_history), params: { ip_history: { ip: @ip_history.ip, note: @ip_history.note, servermaininfo_id: @ip_history.servermaininfo_id } }
    assert_redirected_to ip_history_url(@ip_history)
  end

  test "should destroy ip_history" do
    assert_difference('IpHistory.count', -1) do
      delete ip_history_url(@ip_history)
    end

    assert_redirected_to ip_histories_url
  end
end
