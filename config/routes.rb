Rails.application.routes.draw do
  root 'mains#index'
  resources :users
  resources :roles
  resources :roles_names
  resources :mains
  resources :db_connections
  resources :applications
  resources :important_conf_files
  resources :partitionings
  resources :server_group_infos
  resources :server_states
  resources :os_infos
  resources :machine_types
  resources :site_names
  resources :ip_histories
  resources :server_main_infos do
    get 'treeview', on: :collection
    get 'treeview_item', on: :member
  end
  resources :tests
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
