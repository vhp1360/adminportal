# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_12_07_172818) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "applications", force: :cascade do |t|
    t.string "appName"
    t.string "appType"
    t.string "developer"
    t.string "version"
    t.string "serviceName"
    t.string "appUrl"
    t.string "contextRoot"
    t.integer "dbconnection_id"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "db_connections", force: :cascade do |t|
    t.string "jndiName"
    t.string "dbUserName"
    t.string "dbName"
    t.integer "dbPort"
    t.string "dbIP"
    t.integer "servermaininfo_id"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "important_conf_files", force: :cascade do |t|
    t.string "cfgFileName"
    t.string "filePath"
    t.integer "servermaininfo_id"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "ip_histories", force: :cascade do |t|
    t.integer "servermaininfo_id"
    t.string "ip"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "machine_types", force: :cascade do |t|
    t.string "machineType"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "os_infos", force: :cascade do |t|
    t.string "osName"
    t.string "osVersion"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "partitionings", force: :cascade do |t|
    t.string "prtName"
    t.string "dvcName"
    t.string "totalSpace"
    t.string "usedSpace"
    t.string "totalINode"
    t.string "usedInode"
    t.integer "servermaininfo_id"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "roles", force: :cascade do |t|
    t.integer "rolesname_id"
    t.integer "tablesname_id"
    t.integer "acl"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "whois_id"
  end

  create_table "roles_names", force: :cascade do |t|
    t.string "roleName"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "whois_id"
  end

  create_table "server_group_infos", force: :cascade do |t|
    t.string "grpName"
    t.integer "grp_id"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "server_main_infos", force: :cascade do |t|
    t.string "srvName"
    t.string "fdqn"
    t.string "mainIP"
    t.integer "iphistory_id"
    t.string "macAddr"
    t.string "ip1"
    t.string "ip2"
    t.string "ip3"
    t.string "mac_1"
    t.string "mac_2"
    t.string "mac_3"
    t.string "srvNameInProj"
    t.string "srvPersianName"
    t.integer "sitename_id"
    t.integer "osinfo_id"
    t.string "srvNameOnVM"
    t.integer "machinetype_id"
    t.integer "serverstate_id"
    t.string "cpuInfo"
    t.string "ramInfo"
    t.string "hddInfo"
    t.string "validIP"
    t.string "validFqdn"
    t.string "subnetMask"
    t.string "gateWay"
    t.string "projName"
    t.integer "reponseUnit_id"
    t.integer "responseOrg_id"
    t.string "responseName"
    t.string "responsePhone"
    t.string "responseMail"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "servergroupinfo_id"
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "server_states", force: :cascade do |t|
    t.string "srvState"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "site_names", force: :cascade do |t|
    t.string "siteName"
    t.text "note"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "acl"
    t.integer "whois_id"
  end

  create_table "tests", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "whois_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "firstName"
    t.string "lastName"
    t.integer "userID"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.integer "whois_id"
  end

end
