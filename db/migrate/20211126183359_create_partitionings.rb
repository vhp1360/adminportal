class CreatePartitionings < ActiveRecord::Migration[6.1]
  def change
    create_table :partitionings do |t|
      t.string :prtName
      t.string :dvcName
      t.string :totalSpace
      t.string :usedSpace
      t.string :totalINode
      t.string :usedInode
      t.integer :servermaininfo_id
      t.text :note

      t.timestamps
    end
  end
end
