class AddAclColumnToTables < ActiveRecord::Migration[6.1]
  def change
    add_column :server_main_infos, :acl, :integer
    add_column :ip_histories, :acl, :integer
    add_column :site_names, :acl, :integer
    add_column :os_infos, :acl, :integer
    add_column :machine_types, :acl, :integer
    add_column :server_states, :acl, :integer
    add_column :server_group_infos, :acl, :integer
    add_column :partitionings, :acl, :integer
    add_column :important_conf_files, :acl, :integer
    add_column :applications, :acl, :integer
    add_column :db_connections, :acl, :integer

  end
end
