class CreateServerMainInfos < ActiveRecord::Migration[6.1]
  def change
    create_table :server_main_infos do |t|
      t.string  :srvName
      t.string  :fdqn
      t.string  :mainIP
      t.integer :iphistory_id
      t.string  :macAddr
      t.string  :ip1
      t.string  :ip2
      t.string  :ip3
      t.string  :mac_1
      t.string  :mac_2
      t.string  :mac_3
      t.string  :srvNameInProj
      t.string  :srvPersianName
      t.integer :sitename_id
      t.integer :osinfo_id
      t.string  :srvNameOnVM
      t.integer :machinetype_id
      t.integer :serverstate_id
      t.string  :cpuInfo
      t.string  :ramInfo
      t.string  :hddInfo
      t.string  :validIP
      t.string  :validFqdn
      t.string  :subnetMask
      t.string  :gateWay
      t.string  :projName
      t.integer :reponseUnit_id
      t.integer :responseOrg_id
      t.string  :responseName
      t.string  :responsePhone
      t.string  :responseMail

      t.timestamps
    end
  end
end
