class CreateMachineTypes < ActiveRecord::Migration[6.1]
  def change
    create_table :machine_types do |t|
      t.string :machineType
      t.text :note

      t.timestamps
    end
  end
end
