class CreateSiteNames < ActiveRecord::Migration[6.1]
  def change
    create_table :site_names do |t|
      t.string :siteName
      t.text :note

      t.timestamps
    end
  end
end
