class CreateDbConnections < ActiveRecord::Migration[6.1]
  def change
    create_table :db_connections do |t|
      t.string :jndiName
      t.string :dbUserName
      t.string :dbName
      t.integer :dbPort
      t.string :dbIP
      t.integer :servermaininfo_id
      t.text :note

      t.timestamps
    end
  end
end
