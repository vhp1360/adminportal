class CreateServerStates < ActiveRecord::Migration[6.1]
  def change
    create_table :server_states do |t|
      t.string :srvState
      t.text :note

      t.timestamps
    end
  end
end
