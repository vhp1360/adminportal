class CreateRoles < ActiveRecord::Migration[6.1]
  def change
    create_table :roles do |t|
      t.integer :rolesname_id
      t.integer :tablesname_id
      t.integer :acl

      t.timestamps
    end
  end
end
