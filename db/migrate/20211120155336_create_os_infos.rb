class CreateOsInfos < ActiveRecord::Migration[6.1]
  def change
    create_table :os_infos do |t|
      t.string :osName
      t.string :osVersion
      t.text :note

      t.timestamps
    end
  end
end
