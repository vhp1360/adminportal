class CreateServerGroupInfos < ActiveRecord::Migration[6.1]
  def change
    create_table :server_group_infos do |t|
      t.string :grpName
      t.integer :grp_id
      t.text :note

      t.timestamps
    end
  end
end
