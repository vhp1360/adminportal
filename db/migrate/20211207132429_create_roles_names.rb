class CreateRolesNames < ActiveRecord::Migration[6.1]
  def change
    create_table :roles_names do |t|
      t.string :roleName

      t.timestamps
    end
  end
end
