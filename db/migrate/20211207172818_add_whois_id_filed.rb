class AddWhoisIdFiled < ActiveRecord::Migration[6.1]
  def change
    # [:server_main_infos, :ip_histories, :site_names, :os_infos, ]
    ActiveRecord::Base.connection.tables.each do |t|
      add_column t ,:whois_id, :integer
    end
    ServerGroupInfo.create :grpName => 'Root' , :grp_id => 0
  end
end
