class CreateImportantConfFiles < ActiveRecord::Migration[6.1]
  def change
    create_table :important_conf_files do |t|
      t.string :cfgFileName
      t.string :filePath
      t.integer :servermaininfo_id
      t.text :note

      t.timestamps
    end
  end
end
