class CreateIpHistories < ActiveRecord::Migration[6.1]
  def change
    create_table :ip_histories do |t|
      t.integer :servermaininfo_id
      t.string :ip
      t.text :note

      t.timestamps
    end
  end
end
