class CreateApplications < ActiveRecord::Migration[6.1]
  def change
    create_table :applications do |t|
      t.string :appName
      t.string :appTpe
      t.string :developer
      t.string :version
      t.string :serviceName
      t.string :appUrl
      t.string :contextRoot
      t.integer :dbconnection_id
      t.text :note

      t.timestamps
    end
  end
end
