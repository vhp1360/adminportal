class Application < ApplicationRecord
  belongs_to :server_main_info
  has_many :db_connections

end
