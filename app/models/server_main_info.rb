require 'resolv'
class ServerMainInfo < ApplicationRecord
  belongs_to :os_info
  belongs_to :machine_type
  belongs_to :site_name
  belongs_to :server_state
  belongs_to :os_info
  has_many :ip_histories
  has_many :server_group_infos
  has_many :applications

  validates :srvName, :mainIP, :srvNameInProj, :srvPersianName, :sitename_id, :osinfo_id, :machinetype_id, :serverstate_id, :subnetMask, :gateWay,
            :projName, :responseName, :reponseUnit_id, :responseOrg_id, :responsePhone, presence: true
  validates :mainIP, :ip1, :ip2, :ip3, :validIP, :gateWay, format: { with: Resolv::IPv4::Regex , message: "آدرس شبکه ای دارای قالب درست نمی باشد" }
  validates :macAddr, :mac_1, :mac_2, :mac_3, format: { with: /\A([0-9a-fA-F]{2}:{1}){5}([0-9a-fA-F]{2}){1}\z/i , message: "قالب آدرس کارت شبکه صحیح نمی باشد"}
  validates :responseMail, format: { with: URI::MailTo::EMAIL_REGEXP, message: "قالب آدرس پست الکترونیکی صحیح نمی باشد"}
  validates :subnetMask, numericality: { only_integer: true, less_than_or_equal_to: 32 , greater_than_or_equal_to: 0}
  validates :fdqn, :validFqdn, format: { with: /\A\w+\.\w+\.\w\z/i , message: "لطفا نام شبکه ای را بطور صحیح وارد نمایید"}


end
