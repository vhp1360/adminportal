json.extract! role, :id, :rolesname_id, :tablesname_id, :acl, :whois_id, :created_at, :updated_at
json.url role_url(role, format: :json)
