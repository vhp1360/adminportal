json.extract! server_group_info, :id, :grpName, :grp_id, :note, :created_at, :updated_at
json.url server_group_info_url(server_group_info, format: :json)
