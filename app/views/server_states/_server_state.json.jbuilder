json.extract! server_state, :id, :srvState, :note, :created_at, :updated_at
json.url server_state_url(server_state, format: :json)
