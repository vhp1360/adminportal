json.extract! db_connection, :id, :jndiName, :dbUserName, :dbName, :dbPort, :dbIP, :servermaininfo_id, :note, :created_at, :updated_at
json.url db_connection_url(db_connection, format: :json)
