json.extract! application, :id, :appName, :appTpe, :developer, :version, :serviceName, :appUrl, :contextRoot, :dbconnection_id, :note, :created_at, :updated_at
json.url application_url(application, format: :json)
