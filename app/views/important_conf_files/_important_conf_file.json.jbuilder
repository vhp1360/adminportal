json.extract! important_conf_file, :id, :cfgFileName, :filePath, :servermaininfo_id, :note, :created_at, :updated_at
json.url important_conf_file_url(important_conf_file, format: :json)
