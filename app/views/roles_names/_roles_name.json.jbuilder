json.extract! roles_name, :id, :roleName, :whois_id, :created_at, :updated_at
json.url roles_name_url(roles_name, format: :json)
