json.extract! ip_history, :id, :servermaininfo_id, :ip, :note, :created_at, :updated_at
json.url ip_history_url(ip_history, format: :json)
