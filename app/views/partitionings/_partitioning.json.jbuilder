json.extract! partitioning, :id, :prtName, :dvcName, :totalSpace, :usedSpace, :totalINode, :usedInode, :servermaininfo_id, :note, :created_at, :updated_at
json.url partitioning_url(partitioning, format: :json)
