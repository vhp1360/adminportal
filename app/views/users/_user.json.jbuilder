json.extract! user, :id, :firstName, :lastName, :userID, :whois_id, :created_at, :updated_at
json.url user_url(user, format: :json)
