json.extract! os_info, :id, :osName, :osVersion, :note, :created_at, :updated_at
json.url os_info_url(os_info, format: :json)
