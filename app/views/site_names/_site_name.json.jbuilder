json.extract! site_name, :id, :siteName, :note, :created_at, :updated_at
json.url site_name_url(site_name, format: :json)
