class ServerStatesController < ApplicationController
  before_action :set_server_state, only: %i[ show edit update destroy ]

  # GET /server_states or /server_states.json
  def index
    @server_states = ServerState.all
  end

  # GET /server_states/1 or /server_states/1.json
  def show
  end

  # GET /server_states/new
  def new
    @server_state = ServerState.new
  end

  # GET /server_states/1/edit
  def edit
  end

  # POST /server_states or /server_states.json
  def create
    @server_state = ServerState.new(server_state_params)

    respond_to do |format|
      if @server_state.save
        format.html { redirect_to @server_state, notice: "Server state was successfully created." }
        format.json { render :show, status: :created, location: @server_state }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @server_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /server_states/1 or /server_states/1.json
  def update
    respond_to do |format|
      if @server_state.update(server_state_params)
        format.html { redirect_to @server_state, notice: "Server state was successfully updated." }
        format.json { render :show, status: :ok, location: @server_state }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @server_state.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /server_states/1 or /server_states/1.json
  def destroy
    @server_state.destroy
    respond_to do |format|
      format.html { redirect_to server_states_url, notice: "Server state was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_server_state
      @server_state = ServerState.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def server_state_params
      params.require(:server_state).permit(:srvState, :note)
    end
end
