class PartitioningsController < ApplicationController
  before_action :set_partitioning, only: %i[ show edit update destroy ]

  # GET /partitionings or /partitionings.json
  def index
    @partitionings = Partitioning.all
  end

  # GET /partitionings/1 or /partitionings/1.json
  def show
  end

  # GET /partitionings/new
  def new
    @partitioning = Partitioning.new
  end

  # GET /partitionings/1/edit
  def edit
  end

  # POST /partitionings or /partitionings.json
  def create
    @partitioning = Partitioning.new(partitioning_params)

    respond_to do |format|
      if @partitioning.save
        format.html { redirect_to @partitioning, notice: "Partitioning was successfully created." }
        format.json { render :show, status: :created, location: @partitioning }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @partitioning.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /partitionings/1 or /partitionings/1.json
  def update
    respond_to do |format|
      if @partitioning.update(partitioning_params)
        format.html { redirect_to @partitioning, notice: "Partitioning was successfully updated." }
        format.json { render :show, status: :ok, location: @partitioning }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @partitioning.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partitionings/1 or /partitionings/1.json
  def destroy
    @partitioning.destroy
    respond_to do |format|
      format.html { redirect_to partitionings_url, notice: "Partitioning was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_partitioning
      @partitioning = Partitioning.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def partitioning_params
      params.require(:partitioning).permit(:prtName, :dvcName, :totalSpace, :usedSpace, :totalINode, :usedInode, :servermaininfo_id, :note)
    end
end
