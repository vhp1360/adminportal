class ServerMainInfosController < ApplicationController
  before_action :set_server_main_info, only: %i[ show edit update destroy ]

  # GET /server_main_infos or /server_main_infos.json
  $server_main_infos = ServerMainInfo.all
  def index
  end

  # GET /server_main_infos/1 or /server_main_infos/1.json
  def show
  end

  # TreeView Action
  def treeview
  end

  def treeview_item
    # paginates_per 5
    @servergroupinfo_all_pagination = ServerGroupInfo.order(:grpName).page(params[:page]).per(5)
    @treeviewItem= ServerMainInfo.find(params[:id])
    respond_to do |format|
      format.js { render layout: false }
    end

  end
  # GET /server_main_infos/new
  def new
    @server_main_info = ServerMainInfo.new
    # paginates_per 5
    @servergroupinfo_all_pagination = ServerGroupInfo.order(:grpName).page(params[:page]).per(5)
  end

  # GET /server_main_infos/1/edit
  def edit
    # paginates_per 5
    @servergroupinfo_all_pagination = ServerGroupInfo.order(:grpName).page(params[:page]).per(5)
  end

  # POST /server_main_infos or /server_main_infos.json
  def create
    @server_main_info = ServerMainInfo.new(server_main_info_params)

    respond_to do |format|
      if @server_main_info.save
        format.html { redirect_to @server_main_info, notice: "Server main info was successfully created." }
        format.json { render :show, status: :created, location: @server_main_info }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @server_main_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /server_main_infos/1 or /server_main_infos/1.json
  def update
    # paginates_per 5
    @servergroupinfo_all_pagination = ServerGroupInfo.order(:grpName).page(params[:page]).per(5)
    respond_to do |format|
      if @server_main_info.update(server_main_info_params)
        format.html { redirect_to @server_main_info, notice: "Server main info was successfully updated." }
        format.json { render :show, status: :ok, location: @server_main_info }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @server_main_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /server_main_infos/1 or /server_main_infos/1.json
  def destroy
    @server_main_info.destroy
    respond_to do |format|
      format.html { redirect_to server_main_infos_url, notice: "Server main info was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_server_main_info
      @server_main_info = ServerMainInfo.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def server_main_info_params
      params.require(:server_main_info).permit(:srvName, :fdqn, :mainIP, :iphistory_id, :macAddr, :ip1, :ip2, :ip3, :mac_1, :mac_2, :mac_3, :srvNameInProj, :srvPersianName, :sitename_id, :osinfo_id, :srvNameOnVM, :machinetype_id, :serverstate_id, :cpuInfo, :ramInfo, :hddInfo, :validIP, :validFqdn, :subnetMask, :gateWay, :projName, :reponseUnit_id, :responseOrg_id, :responseName, :responsePhone, :responseMail)
    end
end
