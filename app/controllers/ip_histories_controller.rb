class IpHistoriesController < ApplicationController
  before_action :set_ip_history, only: %i[ show edit update destroy ]

  # GET /ip_histories or /ip_histories.json
  def index
    @ip_histories = IpHistory.all
  end

  # GET /ip_histories/1 or /ip_histories/1.json
  def show
  end

  # GET /ip_histories/new
  def new
    @ip_history = IpHistory.new
  end

  # GET /ip_histories/1/edit
  def edit
  end

  # POST /ip_histories or /ip_histories.json
  def create
    @ip_history = IpHistory.new(ip_history_params)

    respond_to do |format|
      if @ip_history.save
        format.html { redirect_to @ip_history, notice: "Ip history was successfully created." }
        format.json { render :show, status: :created, location: @ip_history }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @ip_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /ip_histories/1 or /ip_histories/1.json
  def update
    respond_to do |format|
      if @ip_history.update(ip_history_params)
        format.html { redirect_to @ip_history, notice: "Ip history was successfully updated." }
        format.json { render :show, status: :ok, location: @ip_history }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @ip_history.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /ip_histories/1 or /ip_histories/1.json
  def destroy
    @ip_history.destroy
    respond_to do |format|
      format.html { redirect_to ip_histories_url, notice: "Ip history was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_ip_history
      @ip_history = IpHistory.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def ip_history_params
      params.require(:ip_history).permit(:servermaininfo_id, :ip, :note)
    end
end
