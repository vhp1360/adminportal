class SiteNamesController < ApplicationController
  before_action :set_site_name, only: %i[ show edit update destroy ]

  # GET /site_names or /site_names.json
  def index
    @site_names = SiteName.all
  end

  # GET /site_names/1 or /site_names/1.json
  def show
  end

  # GET /site_names/new
  def new
    @site_name = SiteName.new
  end

  # GET /site_names/1/edit
  def edit
  end

  # POST /site_names or /site_names.json
  def create
    @site_name = SiteName.new(site_name_params)

    respond_to do |format|
      if @site_name.save
        format.html { redirect_to @site_name, notice: "Site name was successfully created." }
        format.json { render :show, status: :created, location: @site_name }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @site_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /site_names/1 or /site_names/1.json
  def update
    respond_to do |format|
      if @site_name.update(site_name_params)
        format.html { redirect_to @site_name, notice: "Site name was successfully updated." }
        format.json { render :show, status: :ok, location: @site_name }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @site_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /site_names/1 or /site_names/1.json
  def destroy
    @site_name.destroy
    respond_to do |format|
      format.html { redirect_to site_names_url, notice: "Site name was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_site_name
      @site_name = SiteName.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def site_name_params
      params.require(:site_name).permit(:siteName, :note)
    end
end
