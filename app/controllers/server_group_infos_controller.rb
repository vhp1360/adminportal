class ServerGroupInfosController < ApplicationController
  before_action :set_server_group_info, only: %i[ show edit update destroy ]

  # GET /server_group_infos or /server_group_infos.json
  def index
    @server_group_infos = ServerGroupInfo.all
  end

  # GET /server_group_infos/1 or /server_group_infos/1.json
  def show
  end

  # GET /server_group_infos/new
  def new
    @server_group_info = ServerGroupInfo.new
  end

  # GET /server_group_infos/1/edit
  def edit
  end

  # POST /server_group_infos or /server_group_infos.json
  def create
    @server_group_info = ServerGroupInfo.new(server_group_info_params)

    respond_to do |format|
      if @server_group_info.save
        format.html { redirect_to @server_group_info, notice: "Server group info was successfully created." }
        format.json { render :show, status: :created, location: @server_group_info }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @server_group_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /server_group_infos/1 or /server_group_infos/1.json
  def update
    respond_to do |format|
      if @server_group_info.update(server_group_info_params)
        format.html { redirect_to @server_group_info, notice: "Server group info was successfully updated." }
        format.json { render :show, status: :ok, location: @server_group_info }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @server_group_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /server_group_infos/1 or /server_group_infos/1.json
  def destroy
    @server_group_info.destroy
    respond_to do |format|
      format.html { redirect_to server_group_infos_url, notice: "Server group info was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_server_group_info
      @server_group_info = ServerGroupInfo.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def server_group_info_params
      params.require(:server_group_info).permit(:grpName, :grp_id, :note)
    end
end
