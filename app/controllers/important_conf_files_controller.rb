class ImportantConfFilesController < ApplicationController
  before_action :set_important_conf_file, only: %i[ show edit update destroy ]

  # GET /important_conf_files or /important_conf_files.json
  def index
    @important_conf_files = ImportantConfFile.all
  end

  # GET /important_conf_files/1 or /important_conf_files/1.json
  def show
  end

  # GET /important_conf_files/new
  def new
    @important_conf_file = ImportantConfFile.new
  end

  # GET /important_conf_files/1/edit
  def edit
  end

  # POST /important_conf_files or /important_conf_files.json
  def create
    @important_conf_file = ImportantConfFile.new(important_conf_file_params)

    respond_to do |format|
      if @important_conf_file.save
        format.html { redirect_to @important_conf_file, notice: "Important conf file was successfully created." }
        format.json { render :show, status: :created, location: @important_conf_file }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @important_conf_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /important_conf_files/1 or /important_conf_files/1.json
  def update
    respond_to do |format|
      if @important_conf_file.update(important_conf_file_params)
        format.html { redirect_to @important_conf_file, notice: "Important conf file was successfully updated." }
        format.json { render :show, status: :ok, location: @important_conf_file }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @important_conf_file.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /important_conf_files/1 or /important_conf_files/1.json
  def destroy
    @important_conf_file.destroy
    respond_to do |format|
      format.html { redirect_to important_conf_files_url, notice: "Important conf file was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_important_conf_file
      @important_conf_file = ImportantConfFile.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def important_conf_file_params
      params.require(:important_conf_file).permit(:cfgFileName, :filePath, :servermaininfo_id, :note)
    end
end
