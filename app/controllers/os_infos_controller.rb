class OsInfosController < ApplicationController
  before_action :set_os_info, only: %i[ show edit update destroy ]

  # GET /os_infos or /os_infos.json
  def index
    @os_infos = OsInfo.all
  end

  # GET /os_infos/1 or /os_infos/1.json
  def show
  end

  # GET /os_infos/new
  def new
    @os_info = OsInfo.new
  end

  # GET /os_infos/1/edit
  def edit
  end

  # POST /os_infos or /os_infos.json
  def create
    @os_info = OsInfo.new(os_info_params)

    respond_to do |format|
      if @os_info.save
        format.html { redirect_to @os_info, notice: "Os info was successfully created." }
        format.json { render :show, status: :created, location: @os_info }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @os_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /os_infos/1 or /os_infos/1.json
  def update
    respond_to do |format|
      if @os_info.update(os_info_params)
        format.html { redirect_to @os_info, notice: "Os info was successfully updated." }
        format.json { render :show, status: :ok, location: @os_info }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @os_info.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /os_infos/1 or /os_infos/1.json
  def destroy
    @os_info.destroy
    respond_to do |format|
      format.html { redirect_to os_infos_url, notice: "Os info was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_os_info
      @os_info = OsInfo.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def os_info_params
      params.require(:os_info).permit(:osName, :osVersion, :note)
    end
end
