class RolesNamesController < ApplicationController
  before_action :set_roles_name, only: %i[ show edit update destroy ]

  # GET /roles_names or /roles_names.json
  def index
    @roles_names = RolesName.all
  end

  # GET /roles_names/1 or /roles_names/1.json
  def show
  end

  # GET /roles_names/new
  def new
    @roles_name = RolesName.new
  end

  # GET /roles_names/1/edit
  def edit
  end

  # POST /roles_names or /roles_names.json
  def create
    @roles_name = RolesName.new(roles_name_params)

    respond_to do |format|
      if @roles_name.save
        format.html { redirect_to @roles_name, notice: "Roles name was successfully created." }
        format.json { render :show, status: :created, location: @roles_name }
      else
        format.html { render :new, status: :unprocessable_entity }
        format.json { render json: @roles_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /roles_names/1 or /roles_names/1.json
  def update
    respond_to do |format|
      if @roles_name.update(roles_name_params)
        format.html { redirect_to @roles_name, notice: "Roles name was successfully updated." }
        format.json { render :show, status: :ok, location: @roles_name }
      else
        format.html { render :edit, status: :unprocessable_entity }
        format.json { render json: @roles_name.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /roles_names/1 or /roles_names/1.json
  def destroy
    @roles_name.destroy
    respond_to do |format|
      format.html { redirect_to roles_names_url, notice: "Roles name was successfully destroyed." }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_roles_name
      @roles_name = RolesName.find(params[:id])
    end

    # Only allow a list of trusted parameters through.
    def roles_name_params
      params.require(:roles_name).permit(:roleName, :whois_id)
    end
end
